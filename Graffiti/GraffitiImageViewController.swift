//
//  GraffitiImageViewController.swift
//  Graffiti
//
//  Created by Erik Sebastian de Erice on 25/9/16.
//  Copyright © 2016 Erik Sebastián de Erice. All rights reserved.
//

import UIKit

class GraffitiImageViewController: UIViewController {
    
    @IBOutlet weak var graffitiImage: UIImageView!
    var selectedCallout: UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let selectedCallout = selectedCallout {
            graffitiImage.image = selectedCallout
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeModalWindow(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
