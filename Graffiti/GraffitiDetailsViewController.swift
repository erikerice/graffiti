//
//  GraffitiDetailsViewController.swift
//  Graffiti
//
//  Created by Erik Sebastian de Erice on 12/9/16.
//  Copyright © 2016 Erik Sebastián de Erice. All rights reserved.
//

import UIKit
import CoreLocation

protocol GraffitiDetailsViewControllerDelegate: class {
    func graffitiDidFinishGetTagged(sender: GraffitiDetailsViewController, taggedGraffiti: Graffiti)
}

class GraffitiDetailsViewController: UIViewController {
    
    weak var delegate:GraffitiDetailsViewControllerDelegate?
    
    @IBOutlet weak var imgGraffiti: UIImageView!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var taggedGraffiti : Graffiti?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let image = UIImage(named: "img_navbar_title")
        self.navigationItem.titleView = UIImageView(image: image)
        
        let takePictureGesture = UITapGestureRecognizer(target: self, action: #selector(takePictureTapped))
        self.imgGraffiti.addGestureRecognizer(takePictureGesture)
        
        configureLabels()
        
    }
    
    @IBAction func saveGraffiti(_ sender: AnyObject) {
        
        if let image = imgGraffiti.image {
            // Save binary
            let randomName = UUID().uuidString.appending(".png")
            if let url = GraffitiManager.sharedInstance.imagesURL()?.appendingPathComponent(randomName),
               let imageData = UIImagePNGRepresentation(image) {
                do {
                    try imageData.write(to: url)
                } catch (let error){
                    print("Error while saving image: \(error)")
                }
            }
            
            taggedGraffiti = Graffiti(address: addressLabel.text!, latitude: Double(latitudeLabel.text!)!, longitude: Double(longitudeLabel.text!)!, image: randomName)
            
            if let taggedGraffiti = taggedGraffiti {
                delegate?.graffitiDidFinishGetTagged(sender: self, taggedGraffiti: taggedGraffiti)
            }
        }
        
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelPressed(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    func configureLabels() {
        latitudeLabel.text = String(format: "%.8f", (taggedGraffiti?.coordinate.latitude)!)
        longitudeLabel.text = String(format: "%.8f", (taggedGraffiti?.coordinate.longitude)!)
        addressLabel.text = taggedGraffiti?.graffitiAddress
    }

}

extension GraffitiDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func takePictureTapped() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            showPhotoMenu()
        } else {
            choosePhotoFromLibrary()
        }
    }
    
    func showPhotoMenu() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: "Tomar foto", style: .default) { _ in
            self.takePhotoWithCamera()
        }
        alertController.addAction(takePhotoAction)
        
        let chooseFromLibraryAction = UIAlertAction(title: "Elegir de la Librería", style: .default) { _ in
            self.choosePhotoFromLibrary()
        }
        alertController.addAction(chooseFromLibraryAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func takePhotoWithCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    func choosePhotoFromLibrary() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let imageTaken = info[UIImagePickerControllerEditedImage] as? UIImage
        imgGraffiti.image = imageTaken
        saveButton.isEnabled = true
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
}



