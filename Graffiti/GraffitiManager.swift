//
//  GraffitiManager.swift
//  Graffiti
//
//  Created by MIGUEL DIAZ RUBIO on 23/9/16.
//  Copyright © 2016 Erik Sebastián de Erice. All rights reserved.
//

import Foundation

class GraffitiManager {
    
    static let sharedInstance = GraffitiManager()
    
    var graffitis : [Graffiti] = [Graffiti]()
    
    func save() {
        if let url = databaseURL() {
            print("A grabar \(graffitis.count) graffitis")
            print(url.absoluteString)
            NSKeyedArchiver.archiveRootObject(graffitis, toFile: url.path)
        } else {
            print("Error while saving data")
        }
    }
    
    func load() {
        if let url = databaseURL(),
           let savedData = NSKeyedUnarchiver.unarchiveObject(withFile: url.path) as? [Graffiti] {
            graffitis = savedData
        } else {
            print("Error while loading data")
        }
    }
    
    func databaseURL() -> URL? {
        if let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            let url = URL(fileURLWithPath: documentDirectory)
            return url.appendingPathComponent("graffitis.data")
        } else {
            return nil
        }
    }
    
    func imagesURL() -> URL? {
        if let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            let url = URL(fileURLWithPath: documentDirectory)
            return url
        } else {
            return nil
        }
    }
    
}
